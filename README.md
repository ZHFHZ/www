# 该 git 仓库为《铜豌豆 Linux》操作系统官方网站的 git 仓库。

## 网站地址：<https://www.atzlinux.com>

<p>《铜豌豆 Linux》官网二维码<br>
<img src="https://www.atzlinux.com/pics/qr-atzlinux-com.svg" alt="《铜豌豆 Linux》官网二维码">
</p>

<h2>编辑这个 git 仓库</h2>

这个仓库使用了 SSI（Server-side include） 。为了方便，现在集成了一个 python 实现的
支持 SSI 服务器，可以如下操作：

```bash
git clone https://gitee.com/atzlinux/www
cd www
python __utils/ssi_server.py
```

然后，打开 `https://localhost:8000` 就可以看到了！