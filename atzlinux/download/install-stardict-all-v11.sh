#!/bin/bash
apt -y install wget
wget -c -O atzlinux-v11-archive-keyring_lastest_all.deb https://www.atzlinux.com/atzlinux/pool/main/a/atzlinux-archive-keyring/atzlinux-v11-archive-keyring_lastest_all.deb
apt -y install ./atzlinux-v11-archive-keyring_lastest_all.deb
apt update
apt-get -y install festival
apt-get -y install stardict-plugin-spell
apt-get -y install sudo
apt-get -y install stardict-dic-data-atzlinux
apt-get -y install stardict-wyabdcrealpeopletts-atzlinux
apt-get -y install stardict-otdrealpeopletts-atzlinux
