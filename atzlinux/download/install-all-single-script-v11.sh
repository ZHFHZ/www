#!/bin/bash
apt-get -y install wget
wget -c -O atzlinux-v11-archive-keyring_lastest_all.deb https://www.atzlinux.com/atzlinux/pool/main/a/atzlinux-archive-keyring/atzlinux-v11-archive-keyring_lastest_all.deb
apt -y install ./atzlinux-v11-archive-keyring_lastest_all.deb
apt-get update
apt-get -y install xdg-utils
apt-get -y install xfce4-settings
apt-get -y install libcanberra-gtk-module
apt-get -y install electronic-wechat
apt-get -y install linuxqq
apt-get -y install \
fcitx-config-common \
fcitx-config-gtk \
fcitx-frontend-all \
fcitx-frontend-qt5 \
fcitx-googlepinyin \
fcitx-m17n \
fcitx-module-x11 \
fcitx-sunpinyin \
fcitx-table-wubi \
fcitx-table-wbpy \
fcitx-ui-classic
apt-get -y install sogoupinyin
rm -f /etc/apt/sources.list.d/sogoupinyin.list
apt-get -y install fonts-zh-cn-misc-atzlinux
apt-get -y install desktop-file-utils
apt-get -y install baidunetdisk
apt-get -y install netease-cloud-music
apt-get -y install app.web.youdao.dict
apt-get -y install ttf-mscorefonts-atzlinux
apt-get -y install wps-office wps-office-fonts
apt-get -y install atzlinux-store-a11
echo "安装成功，请退出当前登录，重新登录，让安装生效。"
